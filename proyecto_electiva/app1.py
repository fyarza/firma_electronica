# Esto es un comentario, en esta parte de arriba van todas las librerias 


if __name__ == '__main__':

	print("Hola, esta es la funcion main de app1, se ejecutara al compilar el programa\n")
	print("Python no lleva ; al final de cada instruccion ni requiere declaraar variables")
	print("En Python no existen llaves, el scope de las instrucciones se da es por la identacion  \n")
	
	variable1 = True
	
	if variable1 == True:
		print("La variable1 es true..")
		print("Esto es un condicional, antes de cada condicional o ciclo van los puntos :")
		print("Los dos puntos definen el scope, o el alcance de esa instruccion")
		print("Los if y for y while no llevan parentesis\n")
	else:
		print("Esto es el scope del else\n")
	    
	    
	print("Esto se ejecuta despues del if, si se fijan, esta en la misma linea de identacion que el if, pueden borrar esto para comenzar a trabajar\n") 