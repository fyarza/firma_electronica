## Descripción General 


### Construccion de Algoritmo de firma electronica 


Bien, he estado buscando ejemplos de lo que queremos hacer y he encontrado muy buenos, como este: http://www.pythondiario.com/2018/09/cryptography-cifrado-simetrico.html, pero la diferencia es que en este ejemplo esta todo unido, y ademas cifran el documento directamente, es decir el texto plano.

La implementacion que debemos hacer, tiene en medio la gereracion el hash, o la huella del documento y esa huella es la que se cifra. SIn embargo todo lo que haremos basicamente se encuentra alli en ese link utilziando esas libreria.

La idea es hacer este archivo modularizado, y colocar cada codigo, en archivos distintos (App1, App2, App3) y  que se comuniquen mediante archivos, pueden investigar un poco acerca de como leer datos y escribir datos en un archivo en python, es mas facil que en C xD.

## Parte 1
Para la primera parte (app1.py) se pide generar un par de claves utilizando el algoritmo RSA, en el caso de python, ya tiene una libreria que tiene ese algoritmo, se llama cryptography y es lo que hace la primera parte del link.

utilizaremos archivos, de forma tal que, la clave publica se almacene en un archivo "public.txt" y la clave privada se almacene en otro archivo "private.txt"

aqui se genera una clave privada aleatoria, generate_private_key() es una funcion de la libreria rsa, que recibe una serie de parametros, el exponente, el tamaño de la clave en bytes y el backend que es como el tipo de serializacion que utilizará:

private_key = rsa.generate_private_key(
     public_exponent=65537,
     key_size=2048,
     backend=default_backend()
)


aqui se genera una clave publica a partir de la clave privada generada:

public_key = private_key.public_key()

las variables private_key y public_key son las que deben almacenarse en el archivo



## Parte 2
Para la segunda parte (app2.py) se pide firmar un mensaje de texto usando la clave privada del emisor, pienso que acá lo mejor seria que este programa lea del archivo "private.txt" la clave privada y de otro archivo que contenga el mensaje que se quiere enviar en texto plano, llamemosle "mensaje.txt", acá primero se debe construir la huella del mensaje, es decir el hash.
 
en el link, el fragmento de codigo donde esta el comentario de: 
" Firma:
Una clave privada se puede usar para firmar un mensaje. Esto permite a cualquier persona con... "

se hace el proceso de firmado, con el algoritmo sha256(), alli se genera la firma, (la huella hash).

alli mismo en esa parte, mas abajo, se produce el proceso de cifrado, pero ese codigo cifra es el mensaje, no cifra la huella. Nosotros tenemos que cifrar la huella hash,

##### luego de cifrar la huella, esta deberia guardarse en un archivo llamado "huella_cifrada.txt"

## Parte 3 
La parte 3 (app3.py), se debe leer del archivo "public.txt" para extraer la clave publica, "mensaje.txt" para extraer el mensaje  y "huella_cifrada.txt" para extraer la huella cifrada.

este programa debera volver a obtener la huella del mensaje, descifrar la huella obtenida de "huella_cifrada.txt" y comparar ambas para saber si el mensaje es integro o no.



* Propongo que esta parte 3, sea la mia, no se como quieran repartirse la 1 y dos jajaja Mario pudiera hacer la 1 y Federico la 2 o como lo prefieran.
Si quieren proponer cualquier otra cosa pues genial.
* Aca está creada una carpeta la cual contiene los archivos necesarios, todos podemos trabajar en nuestro archivo e ir subiendo los cambios.
* Pueden descargar python para windows o Linux, es este link se explica como dependiendo del caso https://www.python.org/downloads/.
* Para compilar: python nombrearchivo.py
* Se debe instalar criptography, para ello se debe instalar el gestor de paquetes y librerias de python que es pip, aca un link de como hacerlo https://tecnonucleous.com/2018/01/28/como-instalar-pip-para-python-en-windows-mac-y-linux/ 

